import {
	createBottomTabNavigator,
	createAppContainer,
} from 'react-navigation';

import SnapViewScreen from './src/screens/SnapViewScreen';

const AppNavigator = createBottomTabNavigator({
	snapView: SnapViewScreen,
});

export default createAppContainer(AppNavigator);
