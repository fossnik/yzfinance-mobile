import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import {Provider} from 'react-redux';
import {createStoreWithMiddleware} from './src/store/configureStore';

const appWrappedRedux = () => (
	<Provider store={createStoreWithMiddleware}>
		<App/>
	</Provider>
);

AppRegistry.registerComponent(appName, () => appWrappedRedux);
