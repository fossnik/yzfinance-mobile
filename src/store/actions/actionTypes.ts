export const GET_ALL_SNAPSHOTS_FOR_COIN = 'GET_ALL_SNAPSHOTS_FOR_COIN';
export const GET_INDEX_OF_ALL_COINS = 'GET_INDEX_OF_ALL_COINS';
export const SELECT_COIN = 'SELECT_COIN';
export const SELECT_SNAP = 'SELECT_SNAP';
