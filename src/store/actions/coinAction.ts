import {API_URL} from '../../config';
import {GET_INDEX_OF_ALL_COINS, SELECT_COIN} from './actionTypes';
import {TEST_DATA_COIN_INDEX} from '../../../test_data';

const fetchIndexOfAllCoins = () =>
    fetch(`${API_URL}/query_coin_index`)
        .then(response => response.json().then(json => response.ok ? json : Promise.reject(json)))
        .then(response => ({
            type: GET_INDEX_OF_ALL_COINS,
            payload: response,
        }))
        .catch(error => console.error('Could not Load from API\n' + error));

export const getIndexOfAllCoins = () =>
// @ts-ignore
    __DEV__ && TEST_DATA_COIN_INDEX
        ? Promise.resolve({
            type: GET_INDEX_OF_ALL_COINS,
            payload: TEST_DATA_COIN_INDEX,
        })
        : fetchIndexOfAllCoins();

export const selectCoin = coin => ({
    type: SELECT_COIN,
    payload: coin,
});
