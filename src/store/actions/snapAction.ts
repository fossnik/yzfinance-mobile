import {API_URL} from '../../config';
import {GET_ALL_SNAPSHOTS_FOR_COIN, SELECT_SNAP} from './actionTypes';
import {Coin} from '../../interfaces';
import {TEST_DATA_SNAPSHOTS} from '../../../test_data';

const fetchSnapshots = symbol_safe =>
	fetch(`${API_URL}/query_coin/${symbol_safe}`, {
		// @ts-ignore
		headers: __DEV__ ? {limit: '5'} : {},
	})
		.then(response => response.json().then(json => response.ok ? json : Promise.reject(json)))
		.then(response => ({
			type: GET_ALL_SNAPSHOTS_FOR_COIN,
			payload: response,
		}))
		.catch(error => console.error('Could not Load from API\n' + error));

export const getAllSnapshotsForCoin = (coin: Coin) =>
	// @ts-ignore
	__DEV__ && TEST_DATA_SNAPSHOTS[coin.symbol_safe]
		? Promise.resolve({
			type: GET_ALL_SNAPSHOTS_FOR_COIN,
			payload: TEST_DATA_SNAPSHOTS[coin.symbol_safe],
		})
		: fetchSnapshots(coin.symbol_safe);

export const selectSnap = (snap: number) => ({
	type: SELECT_SNAP,
	payload: snap,
});
