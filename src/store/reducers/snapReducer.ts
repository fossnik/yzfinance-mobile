import {GET_ALL_SNAPSHOTS_FOR_COIN, SELECT_SNAP} from '../actions/actionTypes';

const initialState = {
    snapIndex: {},
    selectedSnapNum: 1,
};

export const SnapReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_SNAPSHOTS_FOR_COIN:
            return {
                ...state,
                snapIndex: {
                    ...state.snapIndex,
                    [action.payload.symbol_safe]: action.payload,
                }
            };

        case SELECT_SNAP:
            return {
                ...state,
                selectedSnapNum: action.payload,
            };

        default:
            return state;
    }
};
