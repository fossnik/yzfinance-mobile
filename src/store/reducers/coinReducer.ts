import {GET_INDEX_OF_ALL_COINS, SELECT_COIN} from '../actions/actionTypes';

const initialState = {
    coinIndex: undefined,
    selectedCoin: {
        name: 'Bitcoin USD',
        symbol_full: 'BTC-USD',
        symbol_safe: 'btcusd',
    },
};

export const CoinReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_INDEX_OF_ALL_COINS:
            return {
                ...state,
                coinIndex: action.payload.coins,
            };

        case SELECT_COIN:
            return {
                ...state,
                selectedCoin: action.payload,
            };

        default:
            return state;
    }
};
