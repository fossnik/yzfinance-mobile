import {combineReducers} from 'redux';
import {SnapReducer} from './snapReducer';
import {CoinReducer} from './coinReducer';

const rootReducer = combineReducers({
    SnapReducer,
    CoinReducer,
});

export default rootReducer;
