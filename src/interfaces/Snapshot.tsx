export interface Snapshot {
    ID: number;
    dateCreated: string;
    price: number;
    change: number;
    pChange: number;
    marketCap: number;
    volume: number;
    volume24h: number;
    totalVolume24h: number;
    circulatingSupply: number;
}
