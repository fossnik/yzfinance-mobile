import {Snapshot} from "./Snapshot";

export interface Coin {
    name: string;
    symbol_full: string;
    symbol_safe: string;
}

export interface CoinRecord {
    name: string,
    symbol_safe: string,
    symbol_full: string,
    symbol_icon_url: string,
    snapshots: Snapshot[],
}
