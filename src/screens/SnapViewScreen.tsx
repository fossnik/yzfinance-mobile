import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {Coin, CoinRecord} from '../interfaces';
import {
    SafeAreaView,
    StatusBar,
    Text
} from 'react-native';
import {
    Header,
    CoinMenu,
    SnapshotDetail,
    LoadingSpinner,
    icons,
} from '../components';
import {
    getIndexOfAllCoins,
    selectCoin,
    getAllSnapshotsForCoin,
    selectSnap,
} from '../store/actions';

class SnapViewScreen extends PureComponent<{
    coinIndex: [],
    snapIndex: [],
    selectedCoin: {
        symbol_safe: string,
        symbol_full: string,
        name: string,
    },
    selectedSnapNum: number,
    getIndexOfAllCoins: () => any,
    getAllSnapshotsForCoin: (coin) => any,
    selectCoin: (coin) => any,
    selectSnap: (snap) => any,
}> {
    componentDidMount(): void {
        if (!this.props.coinIndex) {
            this.props.getIndexOfAllCoins()
                .catch(err => console.error({'Error Loading Coin Index': err}));
        }
        if (!this.props.snapIndex[this.props.selectedCoin.symbol_safe]) {
            this.props.getAllSnapshotsForCoin(this.props.selectedCoin)
                .catch(err => console.error({'Error Loading Snapshot': err}));
        }
    }

    onSelectCoin = (coin: Coin): void => {
        if (!this.props.snapIndex[coin.symbol_safe]) {
            this.props.getAllSnapshotsForCoin(coin)
                .then(this.props.selectCoin(coin));
        } else {
            this.props.selectCoin(coin);
        }
    };

    onSelectSnap = (snap: number): void => {
        this.props.selectSnap(snap);
    };

    public render(): JSX.Element {
        const {coinIndex, snapIndex, selectedCoin, selectedSnapNum} = this.props;
        const coinRecord: CoinRecord = snapIndex[selectedCoin.symbol_safe] || null;
        const indexOfSelectedCoin = _.findIndex(coinIndex,(coin: CoinRecord) =>
            coin.symbol_safe === selectedCoin.symbol_safe);

        return (
            <React.Fragment>
                <StatusBar barStyle="dark-content"/>
                <Header/>
                <SafeAreaView style={{flex: 1}}>
                    {
                        coinIndex && selectedCoin &&
                        <CoinMenu
                            onSelectCoin={this.onSelectCoin}
                            coinIndex={coinIndex}
                            selected={{
                              ...selectedCoin,
                              index: indexOfSelectedCoin,
                            }}
                        />
                        || <Text>Loading CoinMenu...</Text>
                    }
                    {
                        coinRecord && selectedSnapNum &&
                        <SnapshotDetail
                            icons={icons}
                            coinRecord={coinRecord}
                            selectedSnapNum={this.props.selectedSnapNum}
                            onSelectSnap={this.onSelectSnap}
                        />
                        || <LoadingSpinner/>
                    }
                </SafeAreaView>
            </React.Fragment>
        );
    }
}

const mapStateToProps = ({CoinReducer, SnapReducer}) => ({
    coinIndex: CoinReducer.coinIndex,
    selectedCoin: CoinReducer.selectedCoin,
    snapIndex: SnapReducer.snapIndex,
    selectedSnapNum: SnapReducer.selectedSnapNum,
});

const mapDispatchToProps = dispatch => ({
    getIndexOfAllCoins: () => dispatch(getIndexOfAllCoins()),
    selectSnap: (snap: number) => dispatch(selectSnap(snap)),
    getAllSnapshotsForCoin: (coin: Coin) => dispatch(getAllSnapshotsForCoin(coin)),
    selectCoin: (coin: string) => dispatch(selectCoin(coin)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SnapViewScreen);
