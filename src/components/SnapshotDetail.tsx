import React from 'react';
import _ from 'lodash';
import {Col, Grid, Row} from 'react-native-easy-grid';
import Colors from './common/Colors';
import {SnapshotSelectDate} from './SnapshotSelectDate';
import {
    Button,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TextStyle,
    View,
    ViewStyle
} from 'react-native';
import {Snapshot, CoinRecord} from "../interfaces";

export function SnapshotDetail(props: {
    icons: {},
    coinRecord: CoinRecord,
    selectedSnapNum: number,
    onSelectSnap: (snap: number) => void,
}): JSX.Element {
    const {name, snapshots, symbol_full, symbol_safe} = props.coinRecord;

    const icon = <Image
        source={props.icons[symbol_safe]}
        style={{width: 60, height: 60}}
    />;

    const {selectedSnapNum} = props;
    const prevSnapNum = _.find(snapshots, ({ID}) => ID === selectedSnapNum - 1)
        && selectedSnapNum - 1 || null;
    const nextSnapNum = _.find(snapshots, ({ID}) => ID === selectedSnapNum + 1)
        && selectedSnapNum + 1 || null;

    const snapData: Snapshot = _.find(snapshots, ({ID}) => ID === selectedSnapNum);

    const {
        price,
        change,
        pChange,
        marketCap,
        volume,
        volume24h,
        totalVolume24h,
        circulatingSupply,
    } = _.mapValues(snapData, prop => prop.toLocaleString());

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic">
            <View style={{
                backgroundColor: Colors.light,
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                paddingVertical: 18,
            }}>
                {icon}
                <View style={{
                    flexDirection: 'column',
                    maxWidth: 250,
                }}>
                    <Text
                        numberOfLines={3}
                        style={{
                            textAlign: 'center',
                            fontSize: 32,
                        }}>{name}</Text>
                    <Text style={{
                        fontSize: 22,
                        textAlign: 'center',
                        color: 'red',
                    }}>{symbol_full}</Text>
                </View>
                {icon}
            </View>
            <Grid>
                <Row style={{
                    backgroundColor: Colors.lighter,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    padding: 4,
                }}>
                    <Col>
                        <View style={styles.navButton}>
                            <Button
                                title='Previous'
                                onPress={() => props.onSelectSnap(prevSnapNum)}
                                disabled={!prevSnapNum}
                            />
                        </View>
                    </Col>
                    <Col>
                        <View>
                            <Text style={{fontSize: 20, textAlign: 'center'}}>{`Snap #${selectedSnapNum}`}</Text>
                            <SnapshotSelectDate
                                onSelectSnap={(snap: number) => props.onSelectSnap(snap)}
                                selectedSnapNum={props.selectedSnapNum}
                                snapshots={snapshots}
                            />
                        </View>
                    </Col>
                    <Col>
                        <View style={styles.navButton}>
                            <Button
                                title='Next'
                                onPress={() => props.onSelectSnap(nextSnapNum)}
                                disabled={!nextSnapNum}
                            />
                        </View>
                    </Col>
                </Row>
            </Grid>
            <Grid style={{
                backgroundColor: Colors.minor,
                flexDirection: 'column',
                justifyContent: 'space-around',
            }}>
                <Row>
                    <Col><Text style={styles.columnTitle}>Price (USD)</Text></Col>
                    <Col><Text style={styles.columnData}>$ {price}</Text></Col>
                </Row>
                <Row>
                    <Col><Text style={styles.columnTitle}>24 Hour Change</Text></Col>
                    <Col>{ snapData.change > 0 ?
                        <Text style={[styles.columnData, {color: 'green'}]}>
                            {`${change} ▲ ${pChange}%`}
                        </Text>
                        : snapData.change < 0 ?
                        <Text style={[styles.columnData, {color: 'red'}]}>
                            {`${change} ▼ ${pChange}%`}
                        </Text>
                        : <Text style={styles.columnData}>0</Text>
                    }</Col>
                </Row>
                <Row>
                    <Col><Text style={styles.columnTitle}>Market Cap</Text></Col>
                    <Col><Text style={styles.columnData}>{marketCap}</Text></Col>
                </Row>
                <Row>
                    <Col><Text style={styles.columnTitle}>Volume</Text></Col>
                    <Col><Text style={styles.columnData}>{volume}</Text></Col>
                </Row>
                <Row>
                    <Col><Text style={styles.columnTitle}>24 Hour Volume</Text></Col>
                    <Col><Text style={styles.columnData}>{volume24h}</Text></Col>
                </Row>
                <Row>
                    <Col><Text style={styles.columnTitle}>24 Hour Total Volume</Text></Col>
                    <Col><Text style={styles.columnData}>{totalVolume24h}</Text></Col>
                </Row>
                <Row>
                    <Col><Text style={styles.columnTitle}>Circulating Supply</Text></Col>
                    <Col><Text style={styles.columnData}>{circulatingSupply}</Text></Col>
                </Row>
            </Grid>
        </ScrollView>
    );
}

const styles = StyleSheet.create<{
    navButton: ViewStyle;
    columnTitle: TextStyle;
    columnData: TextStyle;
}>({
    navButton: {
        margin: 8,
    },
    columnTitle: {
        textAlign: 'right',
        margin: 6,
        fontWeight: 'bold',
    },
    columnData: {
        textAlign: 'left',
        margin: 6,
    },
});
