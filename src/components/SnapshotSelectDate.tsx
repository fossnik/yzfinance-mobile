import React from 'react';
import DatePicker from 'react-native-datepicker';
import _ from 'lodash';
import {Snapshot} from '../interfaces';

const getDate = (dateTime: string) : string => dateTime.split(' ')[0];

export const SnapshotSelectDate = (props: {
    onSelectSnap: (snap: number) => void,
    snapshots: Snapshot[],
    selectedSnapNum: number,
}) => {
    const onDateChange = (date: string) => {
        const snapMatch: Snapshot = _.find(props.snapshots,
			(snap: Snapshot) => snap.dateCreated.startsWith(date));

        if (snapMatch) {
            props.onSelectSnap(snapMatch.ID);
        }
    };

    const snapMatch: Snapshot = _.find(props.snapshots,
        (snap: Snapshot) => snap.ID === props.selectedSnapNum);

    const selectedDate: string = snapMatch ? getDate(snapMatch.dateCreated) : null;

    // @ts-ignore
    const minDate: string = getDate(props.snapshots[0].dateCreated);
    // @ts-ignore
    const maxDate: string = getDate(props.snapshots[props.snapshots.length - 1].dateCreated);

    return selectedDate && minDate && maxDate &&
		<DatePicker
			style={{
				width: 120,
				height: 40,
			}}
			date={selectedDate}
			mode="date"
			format="YYYY-MM-DD"
			minDate={minDate}
			maxDate={maxDate}
			confirmBtnText="Confirm"
			cancelBtnText="Cancel"
			customStyles={{
				dateIcon: {
					width: 30,
					height: 20,
				},
				dateInput: {
					width: 40,
					height: 20,
				},
			}}
			onDateChange={(date: string) => onDateChange(date)}
		/>;
};
