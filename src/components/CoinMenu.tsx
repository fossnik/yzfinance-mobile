import React from 'react';
import SearchableDropdown from 'react-native-searchable-dropdown';
import Colors from './common/Colors';

export const CoinMenu = (props: {
    coinIndex: [],
    onSelectCoin: (coin) => void,
    selected: {
        symbol_safe: string,
        symbol_full: string,
        name: string,
        index: number,
    },
}) => (
    <SearchableDropdown
        onTextChange={
            // On text change listener on the searchable input
            () => void (0)
        }

        onItemSelect={
        	// onItemSelect called after the selection from the dropdown
        	coin => props.onSelectCoin(coin)
        }

        containerStyle={{
            // suggestion container style
            marginHorizontal: 24,
            marginVertical: 16,
        }}

        textInputStyle={{
            // inserted text style
            borderColor: Colors.dark,
            backgroundColor: Colors.white,
            padding: 8,
            borderWidth: 1,
            fontSize: 20,
            textAlign: 'center',
        }}

        itemStyle={{
            // single dropdown item style
            borderColor: Colors.light,
            backgroundColor: Colors.lighter,
            padding: 4,
            marginVertical: 2,
            marginHorizontal: 20,
            borderWidth: 1,
        }}

        itemTextStyle={{
            // text style of a single dropdown item
            color: '#222',
            textAlign: 'center',
        }}

        itemsContainerStyle={{
            // items container style you can pass maxHeight
            // to restrict the items dropdown height
            maxHeight: '60%',
        }}

        items={
            // mapping of item array
            props.coinIndex
        }

        defaultIndex={
            // default selected item index
            props.selected.index
        }

        placeholder={
            // place holder for the search input
            props.selected.name
        }

        // To remove the underline from the android input
        underlineColorAndroid="transparent"
    />
);
