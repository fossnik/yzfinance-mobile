'use strict';

export default {
	white: '#FFF',
	lighter: '#F3F3F3',
	light: '#DAE1E7',
	dark: '#444',
	darkest: '#222',
	black: '#000',
	primary: '#ff3900',
	accent: '#ced08f',
	minor: '#cdffb9',
};
