import React from 'react';
import Spinner from 'react-native-loading-spinner-overlay';
import {StyleSheet, View, ViewStyle} from "react-native";

export const LoadingSpinner = (): JSX.Element =>
    <View style={styles.container}>
        <Spinner
            visible={true}
        />
    </View>;

const styles = StyleSheet.create<{
    container: ViewStyle;
}>({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
});
